#needed:
# sox music player + mp3 libssox
# apcalc calculator
STARTVOLUME=40
VOLMIN=10
#number of files in dir
#ls | wc -l
FILENUM=`ls | grep -v sh | wc -l`
DELTA=`calc "int($STARTVOLUME/$FILENUM)"`
#set master volume to
VOLUME=$STARTVOLUME
xset dpms force off #switch off the screen
amixer -q -D pulse sset Master $VOLUME%
for i in `ls | grep -v sh`
do
  echo "play: $i"
  play $i
  VOLUME=`calc "$VOLUME-$DELTA"`
  if [ "$VOLUME" -ge "$VOLMIN" ]; then
    echo "volume  changed to $VOLUME"
    amixer -q -D pulse sset Master $VOLUME% 
  fi
done
shutdown now
